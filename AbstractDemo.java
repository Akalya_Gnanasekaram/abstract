package bcas.ap.abstrac;

public class AbstractDemo {

	public static void main(String args[]) {
		Person student = new Employee("Thanu", "Female", 113);
		Person employee = new Employee("Akal", "Female", 200);
		student.work();
		employee.work();
		employee.changeName("Akalya");
		System.out.println(employee.toString());

	}

}